#!/usr/bin/env python
# encoding: utf-8
"""Fuzzy matching using multiple algorithms."""
from __future__ import division
import sys
import datetime
import time
import argparse

try:
    from progressbar import ProgressBar, Counter, Percentage, \
        Bar, AdaptiveETA
except ImportError:
    print('Progressbar library not found, please install it first! \
          (https://code.google.com/p/python-progressbar/')

try:
    from slugify import slugify
except ImportError:
    print('Slugify library not found, please install it first! \
          (https://github.com/slugify/slugify')

try:
    from fuzzywuzzy import fuzz
except ImportError:
    print('Fuzzywuzzy library not found, please install it first! \
          (https://github.com/seatgeek/fuzzywuzzy')

try:
    import Levenshtein as levenshtein
except ImportError:
    print('Levenshtein library not found, please install it first! \
          (https://github.com/ztane/python-Levenshtein/')


#################################
# Utility functions
#################################

def int_float(n):
    '''
    Take a number and return an int or float between
    0 and 1 or 0 and 100 to handle scores gracefully
    '''
    if n.find('.') > 0:
        score = float(n)
        if score > 1:
            msg = "Float scores can't be higher than 1!"
            raise argparse.ArgumentTypeError(msg)
    else:
        try:
            score = int(n)
            if score > 100:
                msg = "Int scores can't be higher than 100!"
                raise argparse.ArgumentTypeError(msg)
        except ValueError:
            msg = "Score must be an float between 0 and 1 or an int between \
                    0 and 100!"
            raise argparse.ArgumentTypeError(msg)
    return score


def summary():
    '''Print out a summary of the results'''
    time_end = str(datetime.timedelta(seconds=time.time() - time_start))
    print time_end
    # print '\nMatched {matched:,} items out of {total:,} in {time}'.format(matched=matched,
    #                                                                       total=count,
    #                                                                       time=time_end)


def timestamp():
    return datetime.datetime.now().strftime("%Y%m%d-%H%M%S")


#################################
# Main loop
#################################

def match(file1, file2, file_out):
    with open(file_out, 'w') as out:
        out.write(header)

        with open(file1) as to_match:
            with open(file2) as source_list:
                # Normalize everything just to be sure
                to_match_slugyfied = [
                    slugify(term.strip(), separator=' ')
                    for term in to_match]

                source_list_slugyfied = [
                    slugify(term.strip(), separator=' ')
                    for term in source_list]

                # Initialize counters
                total = len(to_match_slugyfied) * len(source_list_slugyfied)
                count = 0
                matched = 0

                # Set up the progress bar
                widgets=[
                        Percentage(),
                        ' Progress ', Bar(marker='=', left='[', right=']', fill='.'),
                        ' Records: ', Counter(),
                        ' ', AdaptiveETA(100),
                        ]
                pbar = ProgressBar(widgets=widgets, maxval=total, poll=0.01)
                pbar.start()

                # Loop through all terms
                for term in to_match_slugyfied:
                    for item in source_list_slugyfied:
                        terms = [term.encode('utf-8'), item.encode('utf-8')]

                        # Calculate all the scores
                        # Get the function name dynamically
                        for a in algorithms:
                            try:
                                calc_score = getattr(fuzz, a)
                            except AttributeError:
                                calc_score = getattr(levenshtein, a)

                            scores[a] = calc_score(*terms)

                        # Only keep matches above min score
                        if scores[args.algorithm] > minscore:
                            # Normalize and change . to , so excel reads as numbers
                            for k, v in scores.items():
                                if k not in ('jaro_winkler', 'jaro'):
                                    scores[k] = scores[k] / 100
                                scores[k] = str(scores[k]).replace('.', ',')

                            match = ('%s;%s;%s;%s;%s;%s;%s;%s\n') % (term,
                                                                    item,
                                                                    scores['jaro'],
                                                                    scores['jaro_winkler'],
                                                                    scores['ratio'],
                                                                    scores['partial_ratio'],
                                                                    scores['token_sort_ratio'],
                                                                    scores['token_set_ratio'])
                            out.write(match)
                            matched += 1

                        # Show progress to minimize anxiety
                        count += 1
                        pbar.update(count)
                pbar.finish()


if __name__ == '__main__':
    # Handle command line arguments
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('file_to_match')
    parser.add_argument('file_to_be_matched')
    parser.add_argument('--minscore', '-s',
                        required=True,
                        type=int_float,
                        help='minimum score to match')
    parser.add_argument('--algorithm', '-a',
                        required=True,
                        choices=['jaro', 'jaro_winkler',
                                'simple_ratio', 'partial_ratio',
                                'token_sort_ratio', 'token_set_ratio'],
                        help='algorithm to which the min score will be applied to')

    args = parser.parse_args()

    file_out = timestamp() + '_match_results.csv'
    time_start = time.time()
    minscore = args.minscore
    scores = {}
    algorithms = ['jaro', 'jaro_winkler', 'ratio', 'partial_ratio', 'token_sort_ratio', 'token_set_ratio']

    # Allow all int and float scores but convert everything to float
    # Only jaro and jaro-winkler return floats by default
    if args.algorithm in ('jaro', 'jaro-winkler') and minscore > 1:
        minscore = minscore / 100
    elif args.algorithm not in ('jaro', 'jaro-winkler') and minscore < 1:
        minscore = int(minscore * 100)

    # CSV header
    header = '%s;%s;%s\n' % (args.file_to_match, args.file_to_be_matched, ';'.join(algorithms))

    print('Start matching with "%s" and minimum score of %s') % (args.algorithm, minscore)

    try:
        match(args.file_to_match, args.file_to_be_matched, file_out)
    except KeyboardInterrupt:
        sys.exit("Process interrupted!")

    summary()
