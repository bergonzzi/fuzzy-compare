#!/usr/bin/env python
# encoding: utf-8
"""Fuzzy matching using multiple algorithms."""

from __future__ import division
import sys
import math
import time
import argparse
import datetime
import multiprocessing

try:
    from slugify import slugify
except ImportError:
    print('Slugify library not found, please install it first! \
          (https://github.com/slugify/slugify')

try:
    from fuzzywuzzy import fuzz
except ImportError:
    print('Fuzzywuzzy library not found, please install it first! \
          (https://github.com/seatgeek/fuzzywuzzy')

try:
    import Levenshtein as levenshtein
except ImportError:
    print('Levenshtein library not found, please install it first! \
          (https://github.com/ztane/python-Levenshtein/')


# ################################
# Utility functions
# ################################

def int_float(n):
    """
    Take a number and return an int or float between
    0 and 1 or 0 and 100 to handle scores gracefully.
    """
    if n.find('.') > 0:
        score = float(n)
        if score > 1:
            msg = "Float scores can't be higher than 1!"
            raise argparse.ArgumentTypeError(msg)
    else:
        try:
            score = int(n)
            if score > 100:
                msg = "Int scores can't be higher than 100!"
                raise argparse.ArgumentTypeError(msg)
        except ValueError:
            msg = "Score must be an float between 0 and 1 or an int between \
                    0 and 100!"
            raise argparse.ArgumentTypeError(msg)
    return score


def timestamp():
    return datetime.datetime.now().strftime("%Y%m%d-%H%M%S")


def normalize(f):
    """Normalize every line of list f."""
    slugyfied = [
        slugify(term.strip(), separator=' ')
        for term in f]
    return slugyfied


def combine(list1, list2):
    """
    Combine every item of list1 with every item
    of list 2, return list of tuples with pairs.
    """
    result = []
    for x in list1:
        for y in list2:
            result.append([x, y])
    print "Calculated {:,} combinations".format(len(result))
    return result


def score_it(pair):
    """Calculate scores for pair."""
    pname = multiprocessing.current_process().name

    scores = {}
    result = pair

    term1 = pair[0]
    term2 = pair[1]

    # Calculate all the scores
    # Get the function name dynamically
    for a in algorithms:
        try:
            calc_score = getattr(fuzz, a)
        except AttributeError:
            calc_score = getattr(levenshtein, a)

        scores[a] = calc_score(term1, term2)

    # Only keep matches above min score
    if scores[args.algorithm] > minscore:
        # Normalize and change . to , so excel reads as numbers
        for k, v in scores.items():
            if k not in ('jaro_winkler', 'jaro'):
                scores[k] = scores[k] / 100
            scores[k] = str(scores[k]).replace('.', ',')

        result.append(str(scores['jaro']))
        result.append(str(scores['jaro_winkler']))
        result.append(str(scores['ratio']))
        result.append(str(scores['partial_ratio']))
        result.append(str(scores['token_sort_ratio']))
        result.append(str(scores['token_set_ratio']))

        print "{0} found match for {1}".format(pname, pair)
        return result


if __name__ == '__main__':
    # Handle command line arguments
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('file_to_match')
    parser.add_argument('file_to_be_matched')
    parser.add_argument('--minscore', '-s',
                        required=True,
                        type=int_float,
                        help='minimum score to match')
    parser.add_argument('--algorithm', '-a',
                        required=True,
                        choices=['jaro', 'jaro_winkler',
                                 'simple_ratio', 'partial_ratio',
                                 'token_sort_ratio', 'token_set_ratio'],
                        help='algorithm to apply min score to')
    parser.add_argument('--test', action='store_true',
                        help='test run, disables file output')

    args = parser.parse_args()

    file_out = '{0}_match_results.csv'.format(timestamp())
    time_start = time.time()
    minscore = args.minscore
    algorithms = ['jaro', 'jaro_winkler', 'ratio',
                  'partial_ratio', 'token_sort_ratio',
                  'token_set_ratio']

    # Allow all int and float scores but convert everything to float
    # Only jaro and jaro-winkler return floats by default
    if args.algorithm in ('jaro', 'jaro-winkler') and minscore > 1:
        minscore = minscore / 100
    elif args.algorithm not in ('jaro', 'jaro-winkler') and minscore < 1:
        minscore = int(minscore * 100)

    # CSV header
    header = '%s;%s;%s\n' % (
        args.file_to_match,
        args.file_to_be_matched,
        ';'.join(algorithms))

    print "Start matching with minimum score of {0} for {1}".format(
        args.algorithm, minscore)

    try:
        #################################
        # The main stuff
        #################################

        # Files and configuration
        to_match = open(args.file_to_match, "rb").readlines()
        source_list = open(args.file_to_be_matched, "rb").readlines()
        processes = 4
        if not args.test:
            out = open(file_out, 'wb')

        # Normalize files
        print "Normalizing and combining terms..."
        to_match_norm = normalize(to_match)
        source_list_norm = normalize(source_list)
        combined_terms = combine(to_match_norm, source_list_norm)

        # Initialize counters
        total = len(combined_terms)
        matched = 0
        chunksize = 100  # Seems to give the best results

        # Set up multiprocessing
        # Using imap to save on memory
        print "Start matching with chunksize of {0} and {1} processes...".format(chunksize, processes)
        pool = multiprocessing.Pool(processes=processes)
        results = pool.imap(score_it, combined_terms, chunksize=chunksize)

        # Write matches to file
        # Need to check match to leave out None results

        # Write CSV header
        if not args.test:
            out.write(header)

        for match in results:
            if match:
                matched += 1
                if not args.test:
                    line = str(';'.join(match) + '\n')
                    out.write(line)

        try:
            out.close()
        except NameError:
            pass

        pool.close()
        pool.join()

        time_end = str(datetime.timedelta(seconds=time.time() - time_start))
        print "Matched {:,} out of {:,} combinations ({:.2%}) in {}".format(
            matched,
            total,
            (matched / total),
            time_end)
    except KeyboardInterrupt:
        print("Process interrupted! This might look ugly or just hang...")
        try:
            out.close()
        except NameError:
            pass
        pool.terminate()
        sys.exit()
