#!/usr/bin/env python
# encoding: utf-8
"""Fuzzy matching using multiple algorithms."""
from __future__ import division
import os
import sys
import time
import argparse
import datetime
import multiprocessing

try:
    from slugify import slugify
except ImportError:
    print('Slugify library not found, please install it first! \
          (https://github.com/slugify/slugify')

try:
    from fuzzywuzzy import fuzz
except ImportError:
    print('Fuzzywuzzy library not found, please install it first! \
          (https://github.com/seatgeek/fuzzywuzzy')

try:
    import Levenshtein as levenshtein
except ImportError:
    print('Levenshtein library not found, please install it first! \
          (https://github.com/ztane/python-Levenshtein/')


#################################
# Utility functions
#################################

def int_float(n):
    '''
    Take a number and return an int or float between
    0 and 1 or 0 and 100 to handle scores gracefully.
    '''
    if n.find('.') > 0:
        score = float(n)
        if score > 1:
            msg = "Float scores can't be higher than 1!"
            raise argparse.ArgumentTypeError(msg)
    else:
        try:
            score = int(n)
            if score > 100:
                msg = "Int scores can't be higher than 100!"
                raise argparse.ArgumentTypeError(msg)
        except ValueError:
            msg = "Score must be an float between 0 and 1 or an int between \
                    0 and 100!"
            raise argparse.ArgumentTypeError(msg)
    return score


def timestamp():
    return datetime.datetime.now().strftime("%Y%m%d-%H%M%S")


def normalize(file):
    # Normalize everything just to be sure
    slugyfied = [
        slugify(term.strip(), separator=' ')
        for term in file]
    return slugyfied


def combine(list1, list2):
    '''
    Combine every item of list1 with every item of list 2,
    normalize put the pair in the job queue.
    '''
    pname = multiprocessing.current_process().name
    for x in list1:
        for y in list2:
            term1 = slugify(x.strip(), separator=' ')
            term2 = slugify(y.strip(), separator=' ')
            # print("%s is adding '%s - %s' to job queue") % (pname, term1, term2)
            job_queue.put_nowait([term1, term2])


def score_it(job_queue, writer_queue):
    '''Calculate scores for pair of words.'''
    pname = multiprocessing.current_process().name

    for pair in iter(job_queue.get_nowait, "STOP"):
        # print("%s is processing %s") % (pname, str(pair))

        scores = {}
        result = pair

        term1 = pair[0]
        term2 = pair[1]

        # print("%s is scoring '%s' vs '%s'") % (pname, term1, term2)

        # Calculate all the scores
        # Get the function name dynamically
        for a in algorithms:
            try:
                calc_score = getattr(fuzz, a)
            except AttributeError:
                calc_score = getattr(levenshtein, a)

            scores[a] = calc_score(term1, term2)

        # Only keep matches above min score
        if scores[args.algorithm] > minscore:
            # Normalize and change . to , so excel reads as numbers
            for k, v in scores.items():
                if k not in ('jaro_winkler', 'jaro'):
                    scores[k] = scores[k] / 100
                scores[k] = str(scores[k]).replace('.', ',')

            result.append(str(scores['jaro']))
            result.append(str(scores['jaro_winkler']))
            result.append(str(scores['ratio']))
            result.append(str(scores['partial_ratio']))
            result.append(str(scores['token_sort_ratio']))
            result.append(str(scores['token_set_ratio']))

            # print("%s found a match: %s") % (pname, result)
            writer_queue.put(result)
        # else:
        #     print("%s found %s to be below the minscore") % (pname, str(pair))


def writer(writer_queue):
    out = open(file_out, 'wb')
    pname = multiprocessing.current_process().name
    out.write(header)
    for match in iter(writer_queue.get, "STOP"):
        print("%s is writing %s") % (pname, str(match))
        line = str(';'.join(match) + '\n')
        out.write(line)
    out.close()



if __name__ == '__main__':
    #################################
    # Configuration
    #################################
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('file_to_match')
    parser.add_argument('file_to_be_matched')
    parser.add_argument('--minscore', '-s',
                        required=True,
                        type=int_float,
                        help='minimum score to match')
    parser.add_argument('--algorithm', '-a',
                        required=True,
                        choices=['jaro', 'jaro_winkler',
                                'simple_ratio', 'partial_ratio',
                                'token_sort_ratio', 'token_set_ratio'],
                        help='algorithm to which the min score will be applied to')

    args = parser.parse_args()

    file_out = timestamp() + '_match_results.csv'
    time_start = time.time()
    minscore = args.minscore
    algorithms = [
        'jaro', 'jaro_winkler', 'ratio', 'partial_ratio',
        'token_sort_ratio', 'token_set_ratio']

    # Files
    to_match = open(args.file_to_match).readlines()
    source_list = open(args.file_to_be_matched).readlines()

    # Allow all int and float scores but convert everything to float
    # Only jaro and jaro-winkler return floats by default
    if args.algorithm in ('jaro', 'jaro-winkler') and minscore > 1:
        minscore = minscore / 100
    elif args.algorithm not in ('jaro', 'jaro-winkler') and minscore < 1:
        minscore = int(minscore * 100)

    # CSV header
    header = '%s;%s;%s\n' % (
        args.file_to_match,
        args.file_to_be_matched,
        ';'.join(algorithms))


    #################################
    # Main loop
    #################################

    # Set up multiprocessing
    workers = 4
    job_queue = multiprocessing.Manager().Queue()
    writer_queue = multiprocessing.Manager().Queue()
    processes = []

    print('Start matching with "%s", minimum score of %s and %s workers') % (
        args.algorithm, minscore, workers)

    # Normalize files
    # print("Normalizing files...")
    # to_match_norm = normalize(to_match)
    # source_list_norm = normalize(source_list)

    # Fill up job queue
    print("Filling up job queue with term pairs...")
    c = multiprocessing.Process(target=combine, name="Feeder", args=(to_match, source_list))
    c.start()
    c.join()

    print("Job queue size: %s") % job_queue.qsize()

    # Start writer process
    w = multiprocessing.Process(target=writer, name="Writer", args=(writer_queue,))
    w.start()

    for w in xrange(workers):
        p = multiprocessing.Process(target=score_it, args=(job_queue, writer_queue))
        p.start()
        processes.append(p)
        job_queue.put("STOP")

    for p in processes:
        p.join()

    writer_queue.put("STOP")

    print str(datetime.timedelta(seconds=time.time() - time_start))
